package com.astrotv.data;

import android.content.Context;

import com.astrotv.data.db.DbHelper;
import com.astrotv.data.db.DbHelperImpl;
import com.astrotv.data.model.Channel;
import com.astrotv.data.model.ChannelResponse;
import com.astrotv.data.model.EventResponse;
import com.astrotv.data.network.ApiHelper;
import com.astrotv.data.network.AppApiHelper;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public class DataManagerImpl implements DataManager {

    private ApiHelper mApiHelper;
    private DataManagerImpl mInstance;
    private DbHelper dbHelper;
    private Context mContext;

    public DataManagerImpl(Context context) {
        if (mInstance == null) {
            this.mApiHelper = AppApiHelper.getInstance();
            this.mContext = context;
            this.dbHelper = new DbHelperImpl(context);
        }
    }

    @Override
    public Observable<ChannelResponse> getChannelList() {
        return mApiHelper.getChannelList();
    }

    @Override
    public Observable<EventResponse> getEvents(String startTime, String endTime, List<Long> channelId) {
        return mApiHelper.getEvents(startTime, endTime, channelId);
    }

    @Override
    public Observable<Long> insertChannel(Channel channel) {
        return dbHelper.insertChannel(channel);
    }

    @Override
    public Observable<Boolean> insertChannels(List<Channel> channels) {
        return dbHelper.insertChannels(channels);
    }

    @Override
    public Observable<List<Channel>> getFavouriteChannelList() {
        return dbHelper.getFavouriteChannelList();
    }

    @Override
    public Observable<List<Channel>> getAllChanneFromDB() {
        return dbHelper.getAllChanneFromDB();
    }

    @Override
    public void clearDB() {
        dbHelper.clearDB();
    }

    @Override
    public void removeChannel(Channel channel) {
        dbHelper.removeChannel(channel);
    }
}
