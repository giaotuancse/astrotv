/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.astrotv.data.network;

import android.text.TextUtils;

import com.astrotv.data.model.ChannelResponse;
import com.astrotv.data.model.EventResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import java.util.List;

import io.reactivex.Observable;

import static com.astrotv.data.network.ApiEndPoint.QUERY_PARAM_CHANNEL_ID;
import static com.astrotv.data.network.ApiEndPoint.QUERY_PARAM_PERIOD_END;
import static com.astrotv.data.network.ApiEndPoint.QUERY_PARAM_PERIOD_START;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class AppApiHelper implements ApiHelper {

    private static AppApiHelper mInstance;

    public static AppApiHelper getInstance() {
        if (mInstance == null) {
            mInstance = new AppApiHelper();
        }
        return mInstance;
    }

    private AppApiHelper() {
    }

    @Override
    public Observable<ChannelResponse> getChannelList() {
        return Rx2AndroidNetworking.get(ApiEndPoint.END_POINT_GET_CHANNEL_LIST)
                .build()
                .getObjectObservable(ChannelResponse.class);
    }

    @Override
    public Observable<EventResponse> getEvents(String startTime, String endTime, List<Long> channelId) {
        String text = TextUtils.join(",", channelId);
        return Rx2AndroidNetworking.get(ApiEndPoint.END_POINT_GET_EVENTS)
                .addQueryParameter(QUERY_PARAM_CHANNEL_ID,text )
                .addQueryParameter(QUERY_PARAM_PERIOD_START, startTime)
                .addQueryParameter(QUERY_PARAM_PERIOD_END, endTime)
                .build()
                .getObjectObservable(EventResponse.class);
    }


}

