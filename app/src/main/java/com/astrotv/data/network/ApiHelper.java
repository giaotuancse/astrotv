/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.astrotv.data.network;

import com.astrotv.data.model.ChannelResponse;
import com.astrotv.data.model.EventResponse;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public interface ApiHelper {

    Observable<ChannelResponse> getChannelList();
    Observable<EventResponse> getEvents(String startTime, String endTime,  List<Long> channelId);
}
