package com.astrotv.data.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * Created by giaotuan on 11/8/17.
 */

@Entity(nameInDb = "channel")
public class Channel {

    @Id
    private Long channelId;

    @Property(nameInDb = "channel_title")
    private String channelTitle;

    @Property(nameInDb = "channel_stb_number")
    private int channelStbNumber;

    @Property(nameInDb = "is_favourite")
    private boolean favourite;

    @Generated(hash = 1488753559)
    public Channel(Long channelId, String channelTitle, int channelStbNumber,
            boolean favourite) {
        this.channelId = channelId;
        this.channelTitle = channelTitle;
        this.channelStbNumber = channelStbNumber;
        this.favourite = favourite;
    }

    @Generated(hash = 459652974)
    public Channel() {
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true; //if both pointing towards same object on heap
        if (o instanceof Channel) {
            Channel a = (Channel) o;
            return this.getChannelId().equals(a.getChannelId());
        } else {
            return false;
        }
    }

    public Long getChannelId() {
        return channelId;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public int getChannelStbNumber() {
        return channelStbNumber;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public void setChannelStbNumber(int channelStbNumber) {
        this.channelStbNumber = channelStbNumber;
    }
    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public boolean getFavourite() {
        return this.favourite;
    }
}
