package com.astrotv.data.model;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class Event {

    private String eventId;
    private String displayDuration;
    private String displayDateTimeUtc;
    private String displayDateTime;
    private long channelId;

    private long start;
    private long end;

    @SerializedName("programmeTitle")
    private String title;

    public Event(long start, long end, String title) {
        this.start = start;
        this.end = end;
        this.title = title;
    }

    public void initTimeForEPG() {
//        SimpleDateFormat startDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Calendar cal = Calendar.getInstance();
//        TimeZone tz = cal.getTimeZone();
//        startDateFormat.setTimeZone(tz);

        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S").withZone(DateTimeZone.UTC);
        DateTime ldt = DateTime.parse(displayDateTimeUtc, dateTimeFormatter);
        start = ldt.toDateTime(DateTimeZone.getDefault()).toDate().getTime();
        end = start + com.astrotv.utils.DateTimeUtils.getDurationInMilis(displayDuration);

    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public String getTitle() {
        return title;
    }

    public boolean isCurrent() {
        long now = System.currentTimeMillis();
        return now >= start && now <= end;
    }

    public String getEventId() {
        return eventId;
    }

    public String getDisplayDuration() {
        return displayDuration;
    }

    public String getDisplayDateTime() {
        return displayDateTime;
    }

    public long getChannelId() {
        return channelId;
    }
}
