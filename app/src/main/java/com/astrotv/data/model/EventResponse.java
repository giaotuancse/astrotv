package com.astrotv.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class EventResponse {

    @Expose
    @SerializedName("responseCode")
    private String responseCode;

    @Expose
    @SerializedName("responseMessage")
    private String responseMessage;

    @Expose
    @SerializedName("getevent")
    private List<Event> data;

    public String getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public List<Event> getData() {
        return data;
    }
}
