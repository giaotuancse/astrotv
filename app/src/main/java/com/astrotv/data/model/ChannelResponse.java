package com.astrotv.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by giaotuan on 10/31/17.
 */

public class ChannelResponse {
    @Expose
    @SerializedName("responseCode")
    private String responseCode;

    @Expose
    @SerializedName("responseMessage")
    private String responseMessage;

    @Expose
    @SerializedName("channels")
    private List<Channel> data;


    public String getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public List<Channel> getData() {
        return data;
    }
}
