package com.astrotv.data;

import com.astrotv.data.db.DbHelper;
import com.astrotv.data.network.ApiHelper;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public interface DataManager extends ApiHelper, DbHelper {

}
