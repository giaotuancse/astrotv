package com.astrotv.data.db;

import com.astrotv.data.model.Channel;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public interface DbHelper {
    Observable<Long> insertChannel(final Channel channel);
    Observable<Boolean> insertChannels(final List<Channel> channels);
    void removeChannel(final Channel channel);
    Observable<List<Channel>> getFavouriteChannelList();
    Observable<List<Channel>> getAllChanneFromDB();
    void clearDB();

}
