package com.astrotv.data.db;

import android.content.Context;

import com.astrotv.data.model.Channel;
import com.astrotv.data.model.ChannelDao;
import com.astrotv.data.model.DaoMaster;
import com.astrotv.data.model.DaoSession;
import com.astrotv.utils.AppConstants;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public class DbHelperImpl implements DbHelper {

    private DaoSession mDaoSession;

    public DbHelperImpl(Context context) {
        if (mDaoSession == null) {
            mDaoSession = new DaoMaster(new DBOpenHelper(context, AppConstants.DB_NAME).getWritableDb()).newSession();
        }
    }

    @Override
    public Observable<Long> insertChannel(Channel channel) {
        return Observable.fromCallable(() -> mDaoSession.getChannelDao().insertOrReplace(channel));
    }

    @Override
    public Observable<Boolean> insertChannels(final List<Channel> channels) {
        return Observable.fromCallable(() -> {
            mDaoSession.getChannelDao().insertOrReplaceInTx(channels);
            return true;
        });
    }

    @Override
    public void removeChannel(Channel channel) {
        mDaoSession.getChannelDao().delete(channel);
    }

    @Override
    public Observable<List<Channel>> getFavouriteChannelList() {
        return Observable.fromCallable(() -> mDaoSession.getChannelDao().
                queryBuilder().where(ChannelDao.Properties.Favourite.eq(true)).list());
    }

    @Override
    public Observable<List<Channel>> getAllChanneFromDB() {
        return Observable.fromCallable(() -> mDaoSession.getChannelDao().queryBuilder().
                orderAsc(ChannelDao.Properties.ChannelTitle).list());
    }

    @Override
    public void clearDB() {
        mDaoSession.getChannelDao().deleteAll();
    }
}
