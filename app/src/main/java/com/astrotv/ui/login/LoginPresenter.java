package com.astrotv.ui.login;

import com.astrotv.ui.base.MVPPresenter;
import com.astrotv.ui.base.MVPView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public interface LoginPresenter<V extends MVPView> extends MVPPresenter<V> {
    void firebaseAuthWithGoogle(GoogleSignInAccount acct);
}
