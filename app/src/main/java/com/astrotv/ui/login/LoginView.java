package com.astrotv.ui.login;

import com.astrotv.ui.base.MVPView;

/**
 * Created by tuan.giao on 11/12/2017.
 */

public interface LoginView extends MVPView {
    void startHomeScreen();
}
