package com.astrotv.ui.login;

import com.astrotv.R;
import com.astrotv.data.DataManager;
import com.astrotv.data.model.Channel;
import com.astrotv.ui.base.BasePresenter;
import com.astrotv.utils.SchedulerProvider;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;

/**
 * Created by tuan.giao on 11/12/2017.
 */

public class LoginPresenterImpl<V extends LoginView> extends BasePresenter<V> implements LoginPresenter<V> {

    public LoginPresenterImpl(SchedulerProvider schedulerProvider, DataManager dataManager) {
        super(schedulerProvider, dataManager);
    }

    @Override
    public void onViewInitialized() {
        super.onViewInitialized();
    }

    @Override
    public void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        getMvpView().showLoading();
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // Sign in success, update UI with the signed-in user's information
                // get current data direct to homepage
                getFavChannelOfCurrentUser();
            } else {
                // If sign in fails, display a message to the user.
                getMvpView().onError(R.string.error_login_error_failed);
            }
        });
    }

    private void getFavChannelOfCurrentUser() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Channel> channelsList = new ArrayList<>();
                        Iterable<DataSnapshot> s = dataSnapshot.getChildren();
                        for (DataSnapshot item : s) {
                            Channel v = item.getValue(Channel.class);
                            channelsList.add(v);
                        }
                        updateFavChannelsToDB(channelsList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private void updateFavChannelsToDB(@NonNull List<Channel> channelList) {
        getMvpView().hideLoading();
        if (!channelList.isEmpty()) {
            getCompositeDisposable().add(dataManager.insertChannels(channelList)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(aBoolean -> {
                        // go to main screen
                        getMvpView().startHomeScreen();
                    }, throwable -> {
                        getMvpView().onError(throwable.getMessage());
                    }))
            ;
        } else {
            getMvpView().startHomeScreen();
        }
    }
}
