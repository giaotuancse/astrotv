package com.astrotv.ui.epg;

import com.astrotv.data.model.Channel;
import com.astrotv.data.model.Event;

/**
 * Created by Kristoffer on 15-05-25.
 */
public interface EPGClickListener {

    void onChannelClicked(int channelPosition, Channel epgChannel);

    void onEventClicked(int channelPosition, int programPosition, Event epgEvent);

    void onResetButtonClicked();
}
