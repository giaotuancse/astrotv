package com.astrotv.ui.epg;

import com.astrotv.data.model.Channel;
import com.astrotv.data.model.Event;

import java.util.List;

/**
 * Interface to implement and pass to EPG containing data to be used.
 * Implementation can be a simple as simple as a Map/List or maybe an Adapter.
 * Created by Kristoffer on 15-05-23.
 */
public interface EPGData {

    Channel getChannel(int position);

    List<Event> getEvents(int channelPosition);

    Event getEvent(int channelPosition, int programPosition);

    int getChannelCount();

    boolean hasData();

    void addMoreData(List<Channel> channelList, List<Event> eventList);
}
