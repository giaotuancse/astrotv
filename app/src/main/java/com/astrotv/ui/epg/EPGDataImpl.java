package com.astrotv.ui.epg;


import com.astrotv.data.model.Channel;
import com.astrotv.data.model.Event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * TODO: Add null check when fetching at position etc.
 * Created by Kristoffer on 15-05-23.
 */
public class EPGDataImpl implements EPGData {

    private List<Channel> channels = new ArrayList<>();
    private List<List<Event>> events = new ArrayList<>();

    public EPGDataImpl(Map<Channel, List<Event>> data) {
        channels = new ArrayList<>(data.keySet());
        events = new ArrayList<>(data.values());
    }

    public EPGDataImpl(List<Channel> channelList, List<Event> eventList) {
        channels = new ArrayList<>(channelList);
        for (Channel channel : channels) {
            List<Event> eventsOfCurrentChannel = new ArrayList<>();
            for (Iterator<Event> iterator = eventList.iterator(); iterator.hasNext(); ) {
                Event event = iterator.next();
                if (event.getChannelId() == channel.getChannelId()) {
                    event.initTimeForEPG();
                    eventsOfCurrentChannel.add(event);
                    iterator.remove();
                }
            }
            Collections.sort(eventsOfCurrentChannel, (event, t1) -> Long.compare(event.getStart(), t1.getStart()));
            events.add(eventsOfCurrentChannel);
        }
    }

    public Channel getChannel(int position) {
        return channels.get(position);
    }

    public List<Event> getEvents(int channelPosition) {
        return events.get(channelPosition);
    }

    public Event getEvent(int channelPosition, int programPosition) {
        return events.get(channelPosition).get(programPosition);
    }

    public int getChannelCount() {
        return channels.size();
    }

    @Override
    public boolean hasData() {
        return !channels.isEmpty();
    }

    @Override
    public void addMoreData(List<Channel> channelList, List<Event> eventList) {
        channels.addAll(channelList);
        for (Channel channel : channelList) {
            List<Event> eventsOfCurrentChannel = new ArrayList<>();
            for (Iterator<Event> iterator = eventList.iterator(); iterator.hasNext(); ) {
                Event event = iterator.next();
                if (event.getChannelId() == channel.getChannelId()) {
                    event.initTimeForEPG();
                    eventsOfCurrentChannel.add(event);
                    iterator.remove();
                }
            }
            Collections.sort(eventsOfCurrentChannel, (event, t1) -> Long.compare(event.getStart(), t1.getStart()));
            events.add(eventsOfCurrentChannel);
        }
    }
}
