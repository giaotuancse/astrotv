package com.astrotv.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.astrotv.R;
import com.astrotv.data.model.Channel;

/**
 * Created by tuan.giao on 11/10/2017.
 */

public class FavChannelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView tvChannelName;
    private ClickListener clickListener;

    public FavChannelViewHolder(View itemView, ClickListener listener) {
        super(itemView);
        tvChannelName = itemView.findViewById(R.id.tv_channel_name);
    }

    public void bind(@NonNull Channel channel) {
        if (!TextUtils.isEmpty(channel.getChannelTitle())) {
            tvChannelName.setText(channel.getChannelTitle());
        }
    }

    @Override
    public void onClick(View view) {
    }

    public interface ClickListener {
        void onItemClicked(View v, int position);
    }
}
