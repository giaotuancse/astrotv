package com.astrotv.ui.main;

import com.androidnetworking.error.ANError;
import com.astrotv.data.DataManager;
import com.astrotv.data.model.Channel;
import com.astrotv.ui.base.BasePresenter;
import com.astrotv.utils.AlertType;
import com.astrotv.utils.MessageType;
import com.astrotv.utils.SchedulerProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public class MainPresenterImpl<V extends MainView> extends BasePresenter<V> implements MainPresenter<V> {

    private List<Channel> mFavList;
    private DatabaseReference mDatabase;

    public MainPresenterImpl(SchedulerProvider schedulerProvider, DataManager dataManager) {
        super(schedulerProvider, dataManager);
    }

    @Override
    public void onViewInitialized() {
        super.onViewInitialized();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        getCompositeDisposable().add(dataManager.getFavouriteChannelList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(channels -> {
                    mFavList = channels;
                    getMvpView().showListFavouriteChannel(mFavList);
                    doGetChanelList();
                }, throwable -> {
                }));
        getMvpView().updateProfile(mAuth.getCurrentUser());
    }

    @Override
    public void doGetChanelList() {
        getCompositeDisposable().add(dataManager.getChannelList()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(channelResponse -> {
                    if (isViewAttached()) {
                        getMvpView().hideLoading();
                        if (channelResponse != null) {
                            handleListChannelResult(channelResponse.getData());
                        }
                    }
                }, throwable -> {
                    if (!isViewAttached()) {
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        } else {
                            getMvpView().onError(throwable.getMessage());
                        }

                    }
                }));
    }

    @Override
    public void doFavouriteChannel(Channel channel, boolean isFavourite) {
        if (mAuth.getCurrentUser() != null) {
            if (isFavourite) {
                getCompositeDisposable().add(dataManager.insertChannel(channel)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(aLong -> {
                            mDatabase.child(mAuth.getCurrentUser().getUid()).
                                    child(String.valueOf(channel.getChannelId())).setValue(channel, (databaseError, databaseReference) -> {
                                if (databaseError == null) {
                                    getMvpView().addFavouriteChannel(channel);
                                } else {
                                    getMvpView().onError(databaseError.getMessage());
                                }
                            });
                        }, throwable -> {
                            getMvpView().showMessage(throwable.getMessage(), MessageType.SUCCESS, AlertType.TOAST);
                        }));
            } else {
                mDatabase.child(mAuth.getCurrentUser().getUid()).child(String.valueOf(channel.getChannelId()))
                        .removeValue((databaseError, databaseReference) -> {
                            if (databaseError == null) {
                                dataManager.removeChannel(channel);
                                getMvpView().removeFavouriteChannel(channel);
                            } else {
                                getMvpView().onError(databaseError.getMessage());
                            }
                        });

            }
        } else {
            getMvpView().startLoginScreen();
        }


    }


    /**
     * update all channels to ui
     *
     * @param channels list channels fetched from api
     */
    private void handleListChannelResult(List<Channel> channels) {
        if (channels == null || channels.isEmpty()) {
        } else {
            for (Channel channel : channels) {
                if (mFavList.contains(channel)) {
                    channel.setFavourite(true);
                }
            }
            getCompositeDisposable().add(dataManager.insertChannels(channels)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(aLong -> {
                    }, throwable -> {
                    }));
            getMvpView().showListChannel(channels);
        }
    }

    @Override
    public void doLogout() {
        dataManager.clearDB();
        mAuth.signOut();
        getMvpView().startLoginScreen();
    }
}
