package com.astrotv.ui.main;

import com.astrotv.data.model.Channel;
import com.astrotv.ui.base.MVPView;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import io.reactivex.annotations.NonNull;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public interface MainView extends MVPView {
    void showListChannel(@NonNull List<Channel> channelList);

    void showListFavouriteChannel(@NonNull List<Channel> channelList);

    void addFavouriteChannel(@NonNull Channel channel);

    void removeFavouriteChannel(@NonNull Channel channel);

    void startLoginScreen();

    void updateProfile(FirebaseUser user);
}
