package com.astrotv.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrotv.R;
import com.astrotv.data.model.Channel;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class ChannelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView tvChannelName, tvChannelStbNumber;
    private ImageView ivFav;
    private ClickListener clickListener;

    public ChannelViewHolder(View itemView, ClickListener listener) {
        super(itemView);
        tvChannelStbNumber = itemView.findViewById(R.id.tv_channel_stb_number);
        tvChannelName = itemView.findViewById(R.id.tv_channel_name);
        ivFav = itemView.findViewById(R.id.iv_favourite);
        ivFav.setOnClickListener(this);
        clickListener = listener;
    }

    public void bind(@NonNull Channel channel) {
        if (!TextUtils.isEmpty(channel.getChannelTitle())) {
            tvChannelName.setText(channel.getChannelTitle());
        }
        tvChannelStbNumber.setText(String.valueOf(channel.getChannelStbNumber()));
        ivFav.setImageResource(channel.getFavourite() ? R.drawable.ic_favorite_red : R.drawable.ic_favorite_border_black);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_favourite:
                clickListener.onFavouriteClicked(view, getLayoutPosition());
                break;
        }
    }

    public interface ClickListener {

        void onItemClicked(View v, int position);

        void onFavouriteClicked(View v, int position);
    }
}
