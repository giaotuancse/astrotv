package com.astrotv.ui.main;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.astrotv.R;
import com.astrotv.data.model.Channel;
import com.astrotv.utils.RxSyncList;
import com.astrotv.utils.SortUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public class ChannelAdapter extends RecyclerView.Adapter<ChannelViewHolder> implements RxSyncList<Channel> {

    private ChannelViewHolder.ClickListener mListener;
    private List<Channel> mChannels;
    private boolean mSortByNumber;

    public ChannelAdapter(ChannelViewHolder.ClickListener mListener) {
        this.mChannels = new ArrayList<>();
        this.mListener = mListener;
    }

    @Override
    public ChannelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChannelViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_channel_layout, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(ChannelViewHolder holder, int position) {
        Channel buff = mChannels.get(position);
        if (buff != null) {
            holder.bind(buff);
        }
    }

    @Override
    public void add(Channel channel) {
        if (!mChannels.contains(channel)) {
            List<Channel> oldList = new ArrayList<>(mChannels);
            mChannels.add(channel);
            sortAndCallDiff(oldList);
        }
    }

    @Override
    public void add(List<Channel> channels) {
        List<Channel> oldList = new ArrayList<>(mChannels);
        mChannels = SortUtils.sortChannel(mSortByNumber, mChannels);
        mChannels.addAll(channels);
        sortAndCallDiff(oldList);
    }

    @Override
    public void set(Channel channel, int pos) {
        List<Channel> oldList = new ArrayList<>(mChannels);
        mChannels = SortUtils.sortChannel(mSortByNumber, mChannels);
        mChannels.set(pos, channel);
        sortAndCallDiff(oldList);
    }

    @Override
    public void remove(Channel channel) {
        List<Channel> oldList = new ArrayList<>(mChannels);
        mChannels.remove(channel);
        sortAndCallDiff(oldList);
    }

    @Override
    public void remove(int index) {
        List<Channel> oldList = new ArrayList<>(mChannels);
        mChannels.remove(index);
        sortAndCallDiff(oldList);
    }

    @Override
    public void set(List<Channel> channels) {
        List<Channel> oldList = new ArrayList<>(mChannels);
        mChannels.clear();
        mChannels.addAll(channels);
        mChannels = SortUtils.sortChannel(mSortByNumber, mChannels);
        sortAndCallDiff(oldList);
    }

    @Override
    public void clear() {
        int oldSize = mChannels.size();
        mChannels.clear();
        notifyItemRangeRemoved(0, oldSize);
    }

    @Override
    public int getItemCount() {
        return mChannels.size();
    }

    @Override
    public Channel getItem(int position) {
        if (position < mChannels.size()) {
            return mChannels.get(position);
        }
        return null;
    }

    public void toogleSort(boolean isSortByNumber) {
        mSortByNumber = isSortByNumber;
        mChannels = SortUtils.sortChannel(isSortByNumber, mChannels);
        notifyDataSetChanged();
    }

    public void updateSort() {
        mChannels = SortUtils.sortChannel(mSortByNumber, mChannels);
        notifyDataSetChanged();
    }

    private void sortAndCallDiff(List<Channel> oldList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return oldList.size();
            }

            @Override
            public int getNewListSize() {
                return mChannels.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                Channel oldData = oldList.get(oldItemPosition);
                Channel newData = mChannels.get(newItemPosition);
                return oldData.getChannelId().equals(newData.getChannelId());
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                Channel oldData = oldList.get(oldItemPosition);
                Channel newData = mChannels.get(newItemPosition);
                return oldData.getChannelId().equals(newData.getChannelId()) && oldData.getFavourite() == newData.getFavourite();

            }
        });
        diffResult.dispatchUpdatesTo(this);
    }


}
