package com.astrotv.ui.main;

import com.astrotv.data.model.Channel;
import com.astrotv.ui.base.MVPPresenter;

import io.reactivex.annotations.NonNull;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public interface MainPresenter<V extends MainView> extends MVPPresenter<V> {

    void doGetChanelList();

    void doFavouriteChannel(@NonNull Channel channel, boolean isFavourite);

    void doLogout();
}
