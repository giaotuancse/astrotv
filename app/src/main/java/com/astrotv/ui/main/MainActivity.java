package com.astrotv.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.astrotv.R;
import com.astrotv.data.model.Channel;
import com.astrotv.ui.base.BaseActivity;
import com.astrotv.ui.guidetv.GuideTVActivity;
import com.astrotv.ui.login.LoginActivity;
import com.astrotv.utils.Injections;
import com.astrotv.utils.UIUtils;
import com.astrotv.utils.customui.CircleImageView;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import io.reactivex.annotations.NonNull;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView, ChannelViewHolder.ClickListener {

    private MainPresenter mainPresenter;
    private ChannelAdapter mChannelAdapter;
    private FavChannelAdapter mFavChannelAdapter;
    private RecyclerView rvChannelList, rvFabChannelList;
    private CircleImageView ivAvatar;
    private TextView tvName, tvEmail;
    private MenuItem signOutItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenterImpl(Injections.provideSchedulerProvider(),
                Injections.provideAppDataManager(MainActivity.this));
        mainPresenter.onAttach(this);
        mChannelAdapter = new ChannelAdapter(this);
        mFavChannelAdapter = new FavChannelAdapter();
        setUp();
        mainPresenter.onViewInitialized();
    }

    @Override
    protected void setUp() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        signOutItem = navigationView.getMenu().findItem(R.id.sign_out);
        View header = navigationView.getHeaderView(0);
        ivAvatar = header.findViewById(R.id.imageView);
        tvName = header.findViewById(R.id.tvName);
        tvEmail = header.findViewById(R.id.tvEmail);
        rvChannelList = findViewById(R.id.rv_channel_list);
        rvFabChannelList = findViewById(R.id.rv_fav_channel_list);
        rvChannelList.setLayoutManager(new LinearLayoutManager(this));
        rvChannelList.setItemAnimator(new DefaultItemAnimator());
        rvChannelList.setAdapter(mChannelAdapter);
        rvFabChannelList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvFabChannelList.setItemAnimator(new DefaultItemAnimator());
        rvFabChannelList.setAdapter(mFavChannelAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_sort_by_name:
                if (mChannelAdapter != null) {
                    mChannelAdapter.toogleSort(false);
                }
                break;
            case R.id.action_sort_by_number:
                if (mChannelAdapter != null) {
                    mChannelAdapter.toogleSort(true);
                }
                break;
        }
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_gallery) {
            Intent intent = new Intent(MainActivity.this, GuideTVActivity.class);
            startActivity(intent);
        } else if (id == R.id.sign_out) {
            mainPresenter.doLogout();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showListChannel(@NonNull List<Channel> channelList) {
        //  show list channel to adapter
        if (channelList.isEmpty()) {
            //showEmptyContent();
        } else {
            mChannelAdapter.set(channelList);
        }
    }

    @Override
    public void showListFavouriteChannel(List<Channel> channelList) {
        if (channelList.isEmpty()) {
            rvFabChannelList.setVisibility(View.GONE);
        } else {
            rvFabChannelList.setVisibility(View.VISIBLE);
            mFavChannelAdapter.set(channelList);
        }
    }

    @Override
    public void addFavouriteChannel(Channel channel) {
        if (mFavChannelAdapter != null) {
            rvFabChannelList.setVisibility(View.VISIBLE);
            mFavChannelAdapter.add(channel);
            rvFabChannelList.scrollToPosition(mFavChannelAdapter.getItemCount() - 1);
            mChannelAdapter.updateSort();
        }
    }

    @Override
    public void removeFavouriteChannel(Channel channel) {
        if (mFavChannelAdapter != null) {
            mFavChannelAdapter.remove(channel);
            if (mFavChannelAdapter.getItemCount() == 0) {
                rvFabChannelList.setVisibility(View.GONE);
            }
            mChannelAdapter.updateSort();
        }
    }

    @Override
    public void onItemClicked(View v, int position) {

    }

    @Override
    public void onFavouriteClicked(View v, int position) {
        // TODO: 11/1/2017 call to save data
        Channel currentChannel = mChannelAdapter.getItem(position);
        if (currentChannel != null) {
            currentChannel.setFavourite(!currentChannel.getFavourite());
            mainPresenter.doFavouriteChannel(currentChannel, currentChannel.getFavourite());
            mChannelAdapter.notifyItemChanged(position);
        }
    }

    @Override
    public void startLoginScreen() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void updateProfile(FirebaseUser user) {
        if (user != null) {
            signOutItem.setVisible(true);
            if (user.getPhotoUrl() != null) {
                UIUtils.loadImageWithGlide(getApplicationContext(), user.getPhotoUrl().toString(),
                        ivAvatar, android.R.drawable.sym_def_app_icon);
            }
            if (!TextUtils.isEmpty(user.getDisplayName())) {
                tvName.setText(user.getDisplayName());
            }
            if (!TextUtils.isEmpty(user.getEmail())) {
                tvEmail.setText(user.getEmail());
            }
        } else {
            signOutItem.setVisible(false);
        }
    }
}
