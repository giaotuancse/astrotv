/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.astrotv.ui.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.astrotv.utils.AlertType;
import com.astrotv.utils.MessageType;
import com.astrotv.utils.SystemUtil;
import com.astrotv.utils.UIUtils;


/**
 * Created by tuan.giao on 27/07/17.
 */

public abstract class BaseActivity extends AppCompatActivity
        implements MVPView, BaseFragment.Callback {

    private ProgressDialog mProgressDialog;
    private boolean isLife = false;
    private boolean mTurnOnTouchHideKeyBoard = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isLife = true;
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void showLoading() {
        // TODO: 10/31/2017 show loading dialog }
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(BaseActivity.this);
            mProgressDialog.setTitle("Loading...");
        }
        mProgressDialog.show();
    }

    public boolean isActivityStillLive() {
        return isLife;
    }

    @Override
    public void finish() {
        isLife = false;
        super.finish();
    }

    @Override
    public void hideLoading() {
        if (isActivityStillLive()) {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showMessage(message, MessageType.ERROR, AlertType.TOAST);
        }
    }

    @Override
    public void onError(int resId) {
        onError(getString(resId));
    }

    @Override
    public void showMessage(String message, MessageType messageType, AlertType alertType) {
        if (message != null) {
            switch (alertType) {
                case INFO_DIALOG:
                    //  8/15/2017 show dialog here
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(BaseActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(BaseActivity.this);
                    }
                    builder.setTitle("")
                            .setMessage(message)
                            .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                                // continue with delete
                            })
                            .show();
                    break;
                case TOAST:
                    UIUtils.showToast(this, message);
                    break;
                case SNACKBAR:
                    break;
            }
        }
    }

    @Override
    public boolean isNetworkConnected() {
        return SystemUtil.isConnectingToInternet(BaseActivity.this);
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void turnOnTouchHideKeyBoard() {
        mTurnOnTouchHideKeyBoard = true;
    }

    @Override
    public boolean dispatchTouchEvent(final MotionEvent ev) {
        // all touch events close the keyboard before they are processed except
        // EditText instances.
        // if focus is an EditText we need to check, if the touchevent was
        // inside the focus editTexts
        try {
            if (!mTurnOnTouchHideKeyBoard) {
                return super.dispatchTouchEvent(ev);
            }
            final View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                if (!(currentFocus instanceof EditText) || !isTouchInsideView(ev, currentFocus)) {
                    ((InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                    if (currentFocus instanceof EditText) {
                        ((EditText) currentFocus).setCursorVisible(false);
                    }

                }
                return super.dispatchTouchEvent(ev);
            } else {
                return super.dispatchTouchEvent(ev);
            }
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * determine if the given motionevent is inside the given view.
     *
     * @param ev           the given view
     * @param currentFocus the motion event.
     * @return if the given motionevent is inside the given view
     */
    private boolean isTouchInsideView(final MotionEvent ev, final View currentFocus) {
        final int[] loc = new int[2];
        currentFocus.getLocationOnScreen(loc);
        return ev.getRawX() > loc[0] && ev.getRawY() > loc[1] && ev.getRawX() < (loc[0] + currentFocus.getWidth())
                && ev.getRawY() < (loc[1] + currentFocus.getHeight());
    }

    protected abstract void setUp();

    @Override
    public void onTokenExpired() {
        //todo logout
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getFragments() != null && !fm.getFragments().isEmpty()) {
//            fm.popBackStack();
        } else {
            super.onBackPressed();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}
