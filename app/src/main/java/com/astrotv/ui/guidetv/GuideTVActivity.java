package com.astrotv.ui.guidetv;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.astrotv.R;
import com.astrotv.data.model.Channel;
import com.astrotv.data.model.Event;
import com.astrotv.ui.base.BaseActivity;
import com.astrotv.ui.epg.EPG;
import com.astrotv.ui.epg.EPGData;
import com.astrotv.ui.epg.EPGDataImpl;
import com.astrotv.utils.Injections;

import java.util.List;

public class GuideTVActivity extends BaseActivity implements GuideTVView, EPG.OnLoadMoreListener {

    private GuideTVPresenter guideTVPresenter;
    private EPG mEPG;
    private ProgressBar pbLoading;
    EPGData epgData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_tv);
        guideTVPresenter = new GuideTVPresenterImpl(Injections.provideSchedulerProvider(),
                Injections.provideAppDataManager(getApplicationContext()));
        guideTVPresenter.onAttach(this);
        setUp();
        guideTVPresenter.onViewInitialized();
    }

    @Override
    protected void setUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.tv_guide);
        mEPG = findViewById(R.id.epg);
        pbLoading = findViewById(R.id.pbLoading);
        mEPG.setLoadMoreListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void showEPGData(List<Channel> channelList, List<Event> eventList, boolean isAddMore) {
        pbLoading.setVisibility(View.GONE);
        if (isAddMore) {
            epgData.addMoreData(channelList, eventList);
        } else {
            epgData = new EPGDataImpl(channelList, eventList);
        }
        mEPG.setEPGData(epgData);
        mEPG.setLoadingMore(false);
        mEPG.recalculateAndRedraw(false);
    }

    @Override
    public void onLoadMore() {
        pbLoading.setVisibility(View.VISIBLE);
        guideTVPresenter.loadMoreEPG();
    }
}
