package com.astrotv.ui.guidetv;

import com.astrotv.data.model.Channel;
import com.astrotv.data.model.Event;
import com.astrotv.ui.base.MVPView;

import java.util.List;

/**
 * Created by tuan.giao on 11/12/2017.
 */

public interface GuideTVView extends MVPView {

    void showEPGData(List<Channel> channelList, List<Event> eventList, boolean isAddMore);
}
