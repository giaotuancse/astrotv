package com.astrotv.ui.guidetv;

import com.astrotv.ui.base.MVPPresenter;
import com.astrotv.ui.base.MVPView;

/**
 * Created by tuan.giao on 11/12/2017.
 */

public interface GuideTVPresenter<V extends MVPView> extends MVPPresenter<V> {
    void loadMoreEPG();
}
