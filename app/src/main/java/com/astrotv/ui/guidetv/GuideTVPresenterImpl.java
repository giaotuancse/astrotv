package com.astrotv.ui.guidetv;

import com.androidnetworking.error.ANError;
import com.astrotv.data.DataManager;
import com.astrotv.data.model.Channel;
import com.astrotv.ui.base.BasePresenter;
import com.astrotv.ui.epg.EPG;
import com.astrotv.utils.SchedulerProvider;
import com.astrotv.utils.SortUtils;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.astrotv.utils.AppConstants.EPG_PAGING_NUM;

/**
 * Created by tuan.giao on 11/9/2017.
 */

public class GuideTVPresenterImpl<V extends GuideTVView> extends BasePresenter<V> implements GuideTVPresenter<V> {

    private List<Channel> channelList;
    private int indexChannelList;

    public GuideTVPresenterImpl(SchedulerProvider schedulerProvider, DataManager dataManager) {
        super(schedulerProvider, dataManager);
        channelList = new ArrayList<>();
    }

    @Override
    public void onViewInitialized() {
        super.onViewInitialized();
        getCompositeDisposable().add(dataManager.getAllChanneFromDB()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(channels -> {
                    SortUtils.sortChannel(false, channels);
                    channelList = channels;
                    loadEvents();
                }, throwable -> {
                })
        );
    }

    private void loadEvents() {
        List<Long> channel = new ArrayList<>();
        List<Channel> currentChanels = new ArrayList<>();
        for (int i = 0; i < EPG_PAGING_NUM; i++) {
            if (indexChannelList < channelList.size()) {
                channel.add(channelList.get(indexChannelList).getChannelId());
                currentChanels.add(channelList.get(indexChannelList));
                indexChannelList++;
            }
        }
        //long currentTime = System.currentTimeMillis();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        df.setTimeZone(TimeZone.getTimeZone("GMT+8"));

        DateTime date = new DateTime(org.joda.time.DateTimeZone.UTC);
        long startTime = date.getMillis() - EPG.DAYS_BACK_MILLIS;
        long endTime = date.getMillis() + EPG.DAYS_FORWARD_MILLIS;

        getCompositeDisposable().add(dataManager.getEvents(df.format(new Date(startTime)), df.format(new Date(endTime)), channel)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(eventResponse -> {
                    if (isViewAttached()) {
                        getMvpView().showEPGData(currentChanels, eventResponse.getData(), indexChannelList > 10);
                    }
                }, throwable -> {
                    //  11/6/2017 handle error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                    } else {
                        getMvpView().onError(throwable.getMessage());
                    }

                }));

    }

    @Override
    public void loadMoreEPG() {
        loadEvents();
    }
}
