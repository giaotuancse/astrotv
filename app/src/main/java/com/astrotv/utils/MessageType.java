package com.astrotv.utils;

/**
 * Created by tuan.giao on 7/10/2017.
 */

public enum MessageType {
    ERROR, SUCCESS, INFO
}
