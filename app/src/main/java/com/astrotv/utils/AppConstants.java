package com.astrotv.utils;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class AppConstants {

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;
    public static final int EPG_PAGING_NUM = 10;

    public static final String DB_NAME = "astrotv.db";
}
