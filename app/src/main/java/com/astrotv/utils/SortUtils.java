package com.astrotv.utils;

import com.astrotv.data.model.Channel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by tuan.giao on 11/12/2017.
 */

public class SortUtils {

    private static Comparator<Channel> stbNumberComparator = (object1, object2) -> {
        if ((object1.getFavourite() && object2.getFavourite()) || (!object1.getFavourite() && !object2.getFavourite())) {
            return object1.getChannelStbNumber() - object2.getChannelStbNumber();
        } else {
            return (object1.getFavourite() ? -1 : 1);
        }

    };

    private static Comparator<Channel> nameComparator = (object1, object2) -> {
        if ((object1.getFavourite() && object2.getFavourite()) || (!object1.getFavourite() && !object2.getFavourite())) {
            return object1.getChannelTitle().compareTo(object2.getChannelTitle());
        } else {
            return (object1.getFavourite() ? -1 : 1);
        }
    };

    public static List<Channel> sortChannel(boolean isSortByNumber, List<Channel> channelList) {
        Collections.sort(channelList, isSortByNumber ? stbNumberComparator : nameComparator);
        return channelList;
    }
}
