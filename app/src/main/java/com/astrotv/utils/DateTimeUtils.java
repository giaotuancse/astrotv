package com.astrotv.utils;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class DateTimeUtils {

    public static long getDurationInMilis(String time) {
        String[] tokens = time.split(":");
        int minutesToMs = Integer.parseInt(tokens[1]) * 60000;
        int hoursToMs = Integer.parseInt(tokens[0]) * 3600000;
        return minutesToMs + hoursToMs;
    }
}
