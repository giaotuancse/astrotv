/*
 */

package com.astrotv.utils;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;


/**
 * An assortment of UI helpers.
 */
public class UIUtils {

    public static void showToast(Context context, int aligment, String message,
                                 int time) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static void showDebugToast(Context context, String message) {
        showToast(context, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
                message, 1);
    }

    public static void showToast(Context context, String message) {
        showToast(context, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
                message, 1);
    }

    public static void showToastBottom(Context context, String message) {
        showToast(context, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, message,
                0);
    }

    public static void showToast(Context context, String message, int time) {
        showToast(context, Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,
                message, time);
    }


    public static void loadImageWithGlide(Context context, String url, ImageView imageView, @DrawableRes int imageResourceHolder) {
        if (context != null) {
            Glide.with(context).load(url).placeholder(imageResourceHolder)
                    .error(imageResourceHolder)
                    .dontAnimate().into(imageView);
        } else {
            imageView.setImageResource(imageResourceHolder);
        }
    }

    public static void loadImageWithGlide(Context context, String url, ImageView imageView, @DrawableRes int imageResourceHolder, @DrawableRes int imageResourceErrorHolder) {
        if (context != null) {
            Glide.with(context).load(url).placeholder(imageResourceHolder)
                    .error(imageResourceErrorHolder)
                    .dontAnimate()
                    .into(imageView);
        } else {
            imageView.setImageResource(imageResourceHolder);
        }
    }

}
