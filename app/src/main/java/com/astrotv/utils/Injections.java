package com.astrotv.utils;

import android.content.Context;
import android.support.annotation.NonNull;

import com.astrotv.data.DataManager;
import com.astrotv.data.DataManagerImpl;

/**
 * Created by tuan.giao on 11/8/2017.
 */

public class Injections {

    public static DataManager provideAppDataManager(@NonNull Context context) {
        return new DataManagerImpl(context);
    }

    public static SchedulerProvider provideSchedulerProvider() {
        return AppSchedulerProvider.getInstance();
    }
}
